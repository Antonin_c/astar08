<div class="footer-entier">
<footer>
    <div class="gauche">
        <ul class="list-lien">
            <div class="top">
                <li class="list-footer">
                    <a href="accueil.php">▶ Accueil</a>
                    <a class="col2" href="accueil_forum.php">▶ Le forum</a>
                </li>
                <li class="list-footer">
                    <a href="boutique.php">▶ Notre boutique</a>
                    <a class="col3" href="#">▶ Nos tournois</a>
                </li>
            </div>
            <div class="bot">
                <li class="list-footer"><a href="#">Nous contacter</a></li>
                <li class="list-footer"><a href="#">Politique de confidentialité</a></li>
            </div>
        </ul>
    </div>
    <div class="centre">
            <div class="titre-follow">
                <p class="follow">Suivez-nous</p>
            </div>
            <div>
                <hr class="line1">
                <div class="social">
                    <a href="http://www.instagram.com/" class="instagram"><img src="immage/insta.png" height="100%" width="100%"/></a>
                    <a href="http://www.discord.com/" class="discord"><img src="immage/di.png" height="100%" width="100%"/></a>
                    <a href="http://www.twitter.com/"  class="twitter"><img src="immage/Tw.png" height="100%" width="100%"/></a>
                    <a href="https://fr-fr.facebook.com/people/Sylvain-Durif/100076405766790/" class="facebook"><img src="immage/fac.png" height="100%" width="100%"/></a>
                </div>
                <hr class="line2">
                <p class="copyright">Association Asetar08 © 2022</p>
            </div>

    </div>
    <div class="droite">
        <div class="about">
            <p>A Propos :</p><br>
                <dl> Nous sommes une association consistant à vendre pour collecter des fonds à but non lucratif</dl>
        </div>
        <select class="langue" name="nom" size="1">
            <option>Français
            <option>Anglais
        </select>
    </div>
</footer>
</div>
</body>
</html>