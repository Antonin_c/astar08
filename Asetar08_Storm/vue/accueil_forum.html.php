<?php

include_once $racine.path_os("/modele/bd_forum.php");
$result = home();
$result2 = change_time();

echo "
<div class='bar_forum'>
    <div >
    <a class='button_nouveau_sujet' href='./nouveau_sujet.html.php'>SUJET</a>
    </div>
    <div class='sujet_forum_entete'>
    SUJET
    </div>
    <div class='auteur_forum_entete'>
    AUTEUR
    </div>
    <div class='nb_message_forum_entete'>
    MESSAGES
    </div>
    <div class='date_forum_entete'>
    DATE
    </div>
</div>
";


foreach ($result as $data)
{
    echo "<div class='forum'>";
    // on décompose la date
    sscanf($data['date_derniere_reponse'], "%4s-%2s-%2s %2s:%2s:%2s", $annee, $mois, $jour, $heure, $minute, $seconde);
    echo "<div class='sujet_forum'>";
    // on affiche le titre du sujet et un lien qui nous permeterra de voir les reponses de ce sujet
    echo  '<a href="./lire_sujet.html.php?id_sujet_a_lire=', $data["id"], '">', $data["sujet"],'</a>';
    echo "</div>";
    echo "<div class='auteur_forum'>";
    echo $data['auteur']; // on affiche le nom de l'auteur
    echo "</div>";
    echo "<div class='date_forum'>";
    // on affiche la date de la dernière réponse de chaque sujet
    echo $jour , '-' , $mois , '-' , $annee , ' ' , $heure , ':' , $minute;
    echo "</div>";
    echo "</div>";
}
?>