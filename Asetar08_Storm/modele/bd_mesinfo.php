<?php

include_once $racine.path_os("/modele/bd_conn.php");
include_once $racine.path_os("/modele/bd_creecompte.php");

/**
 * @throws Exception
 */
function get_info_from_email($email)
{
    try
    {
        $db = new database();
        $db->exec("SELECT * FROM compte AS c WHERE c.adressemail = '".$email."'");
        $result = $db->result();
        return $result;
    }
    catch (Exception $e)
    {
        var_dump($e);
        throw new Exception($e);
    }
}


/**
 * @throws Exception
 */
function get_id_from_email($email)
{
    try
    {
        $db = new database();
        $db->exec('select id from compte where adressemail = "'.$email.'"');
        $r = $db->result();
        return $r[0]["id"];
    }
    catch (Exception $e)
    {
        var_dump($e);
        throw new Exception($e);
    }
}


function set_info_where_id($info, $info_name, $id): bool
{
    try
    {
        $db = new database();
        $db->exec('update compte set '.$info_name.' = "'.$info.'" where id = "'.$id.'"');
        return true;
    }
    catch (Exception $e)
    {
        var_dump($e);
        throw new Exception($e);
    }
}
