<?php

include_once $racine.path_os("/modele/bd_conn.php");

/**
 * @throws Exception
 */
function get_all_info_user()
{
    try
    {
        $db = new database();
        $db->exec("SELECT id, adressemail, nom, prenom, numero_tel, grade FROM compte");
        $result = $db->result();
        return $result;
    }
    catch (Exception $e)
    {
        var_dump($e);
        throw new Exception($e);
    }
}