<?php

include_once $racine.path_os("/modele/bd_conn.php");

/**
 * @throws Exception
 */
function get_all_news()
{
    try
    {
        $db = new database();
        $db->exec("SELECT date_publication, titre, path_immage, path_texte FROM news ORDER BY date_publication DESC;");
        $result = $db->result();
        return $result;
    }
    catch (Exception $e)
    {
        throw new Exception($e);
    }
}
?>