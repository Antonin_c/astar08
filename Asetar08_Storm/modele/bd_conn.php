<?php

    class database
    {
        private $user;
        private $password;
        private $db_name;
        private $host;
        private $conn;
        private $curs;

        /**
         * @throws Exception
         */
        public function __construct($user = "user1", $password = "123456789", $db_name = "database_Asetar08", $host = "localhost")
        {
            try
            {
                $this->user = $user;
                $this->password = $password;
                $this->db_name = $db_name;
                $this->host = $host;
                $this->conn = new PDO("mysql:host=".$host.";dbname=".$db_name, $user, $password);
                return true;
            }
            catch (Exception $e)
            {
                var_dump($e);
                return $e;
            }

        }

        /**
         * @throws Exception
         */
        public function exec($cmd)
        {
            try
            {
                $this->curs = $this->conn->prepare($cmd);
                $this->curs->execute();
                return true;
            }
            catch (Exception $e)
            {
                var_dump($e);
                throw new Exception($e);
                return false;
            }
        }

        /**
         * @throws Exception
         */
        public function result()
        {
            try
            {
                return $this->curs->fetchAll(PDO::FETCH_ASSOC);
            }
            catch (Exception $e)
            {
                var_dump($e);
                throw new Exception($e);
                return false;
            }
        }
    }
?>