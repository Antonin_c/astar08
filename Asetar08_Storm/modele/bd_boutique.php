<?php


include_once $racine.path_os("/modele/bd_conn.php");

/**
 * @throws Exception
 */
function get_all_article()
{
    try
    {
        $db = new database();
        $db->exec("SELECT id, nom, prix, path_immage, description FROM article");
        $result = $db->result();
        return $result;
    }
    catch (Exception $e)
    {
        throw new Exception($e);
    }
}

/**
 * @throws Exception
 */
function get_name_from_id($id)
{
    try
    {
        $db = new database();
        $db->exec("SELECT nom FROM article WHERE id = '".$id."'");
        $result = $db->result();
        return $result[0]["nom"];
    }
    catch (Exception $e)
    {
        throw new Exception($e);
    }
}


/**
 * @throws Exception
 */
function get_prix_from_id($id)
{
    try
    {
        $db = new database();
        $db->exec("SELECT prix FROM article WHERE id = '".$id."'");
        $result = $db->result();
        return $result[0]["prix"];
    }
    catch (Exception $e)
    {
        throw new Exception($e);
    }
}