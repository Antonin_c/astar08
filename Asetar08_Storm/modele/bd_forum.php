<?php

include_once $racine.path_os("/modele/bd_conn.php");

/**
 * @throws Exception
 */

function home()
{
    try
    {
        $db = new database("user1", "123456789", "BD_FORUM", "localhost");
        $db->exec("SELECT id, auteur, sujet, date_derniere_reponse FROM forum_sujets ORDER BY date_derniere_reponse DESC;");
        $result = $db->result();
        return $result;
    }
    catch (Exception $e)
    {
        throw new Exception($e);
    }
}

function reading()
{
    try
    {
        $db = new database("user1", "123456789", "BD_FORUM", "localhost");
        $db->exec('SELECT id, auteur, message, date_reponse FROM forum_reponses WHERE correspondance_sujet = "'.$_GET['id_sujet_a_lire'].'" ORDER BY date_reponse ASC;');
        $result = $db->result();
        return $result;
    }
    catch (Exception $e)
    {
        throw new Exception($e);
    }
}

function get_id_from_email($email)
{
    try
    {
        $db = new database();
        $db->exec('select nom, prenom from compte where adressemail = "'.$email.'"');
        $r = $db->result();
        return $r[0];
    }
    catch (Exception $e)
    {
        throw new Exception($e);
    }
}

function get_all_topic()
{
    // on appelle la fonction contenant un id par rapport à un email
    $r = get_id_from_email($_SESSION["email"]);

    // Si le titre a été saisi dans le champ de texte on exécute la requête SQL
    if (isset($_POST["sujet"]))
    {
        try
        {
            $date = date("Y-m-d H:i:s" );
            $db = new database("user1", "123456789", "BD_FORUM", "localhost");
            $db->exec('INSERT INTO forum_sujets (auteur, sujet, date_derniere_reponse) VALUES("'.$r["nom"]." ".$r["prenom"].'", "'.$_POST["sujet"].'", "'.$date.'")');
        }
        catch (Exception $e)
        {
            throw new Exception($e);
        }
    }
}

function get_all_topic2()
{
    // on appelle la fonction contenant un id par rapport à un email
    $r2 = get_id_from_email($_SESSION["email"]);

    // Si le premier message a été saisi dans le champ de texte on exécute la requête SQL
    if (isset($_POST["message"]))
    {
        try
        {
            $date = date("Y-m-d H:i:s" );
            $db = new database("user1", "123456789", "BD_FORUM", "localhost");
            $db->exec('SELECT id FROM forum_sujets WHERE auteur = "'.$r2["nom"]." ".$r2["prenom"].'" AND sujet = "'.$_POST["sujet"].'" AND date_derniere_reponse = "'.$date.'"');
            $id_sujet = $db ->result();
            $db->exec('INSERT INTO forum_reponses (auteur, message, date_reponse, correspondance_sujet) VALUES("'.$r2["nom"]." ".$r2["prenom"].'", "'.$_POST["message"].'", "'.$date.'", '.$id_sujet[0]["id"].')');
        }
        catch (Exception $e)
        {
            throw new Exception($e);
        }
    }
}

function get_all_answers()
{
    $r3 = get_id_from_email($_SESSION["email"]);

    if(isset($_POST["reponse"]))
    {
        try
        {
            $date = date("Y-m-d H:i:s" );
            $db = new database("user1", "123456789", "BD_FORUM", "localhost");
            $db->exec('INSERT INTO forum_reponses (auteur, message, date_reponse, correspondance_sujet) VALUES("'.$r3["nom"]." ".$r3["prenom"].'", "'.$_POST["reponse"].'", "'.$date.'", '.$_GET["id_sujet_a_lire"].')');
        }
        catch (Exception $e)
        {
            throw new Exception($e);
        }
    }
}

function change_time()
{
    try
    {
        $date = date("Y-m-d H:i:s" );
        $db = new database("user1", "123456789", "BD_FORUM", "localhost");
        $db->exec('SELECT id FROM forum_sujets');
        $id_sujet = $db ->result();
        $db->exec('UPDATE forum_sujets SET date_derniere_reponse="'.$date.'" WHERE id= '.$id_sujet[0]["id"].' ');
    }
    catch (Exception $e)
    {
        throw new Exception($e);
    }
}


?>