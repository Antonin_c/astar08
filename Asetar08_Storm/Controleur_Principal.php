<?php

#GPL V3

include_once "os_compatibility.php";
$domaine = "http://www.asetar08.org:8080";
$racine = dirname(__FILE__);
$req_uri = $_SERVER[REQUEST_URI];

#Envoie d'un cookies si il n'y en a pas
if (!isset($_SESSION)) {
    session_start();
}

#Deconecte l'utilisateur si il a envoier une demande via la methode posty
if (isset($_POST)) {
    if (isset($_POST["deco"])) {
        if ($_POST["deco"] == "SE DECONNECTER") {
            unset($_SESSION["email"]);
        }
    }
}

#Apelle le controleur de la page demander dans l'url
switch ($req_uri) {
    case "/":
    case "/accueil.php":
    case "/accueil.php?":
        include_once $racine . path_os("/controleur/accueil.php");
        break;
    case "/login.php":
    case "/login.php?":
        include_once $racine . path_os("/controleur/login.php");
        break;
    case "/motsdepassoublie.php":
    case "/motsdepassoublie.php?":
        include_once $racine . path_os("/controleur/motsdepassoublie.php");
        break;
    case "/creecompte.php":
    case "/creecompte.php?":
        include_once $racine . path_os("/controleur/creecompte.php");
        break;
    case "/mesinfo.php":
    case "/mesinfo.php?":
        include_once $racine . path_os("/controleur/mesinfo.php");
        break;
    case "/boutique.php":
    case "/boutique.php?":
        include_once $racine . path_os("/controleur/boutique.php");
        break;
    case "/panier.php":
    case "/panier.php?":
        include_once $racine . path_os("/controleur/panier.php");
        break;
    case "/accueil_forum.php":
    case "/accueil_forum.php?":
        include_once $racine . path_os("/controleur/accueil_forum.php");
        break;
    case "/paneladmin.php":
    case "/paneladmin.php?":
        include_once $racine . path_os("/controleur/paneladmin.php");
        break;
    case substr( $req_uri, 0, 21 ) === "/lire_sujet.html.php?";
        include_once $racine.path_os("/controleur/lire_sujet.php");
        break;
    case substr( $req_uri, 0, 23 ) === "/nouveau_sujet.html.php";
        include_once $racine.path_os("/controleur/nouveau_sujet.php");
        break;
    default:
        if (substr($req_uri, 0, 13) === "/news/immage/") {
            #!!! Faille de securité
            include_once $racine . path_os("/controleur/immage.php"); #!!!
            #!!!
        }
        if (substr($req_uri, 0, 16) === "/article/immage/") {
            #!!! Faille de securité
            include_once $racine . path_os("/controleur/immage.php"); #!!!
            #!!!
        }
        if (substr($req_uri, 0, 8) === "/immage/") {
            #!!! Faille de securité
            include_once $racine . path_os("/controleur/immage.php"); #!!!
            #!!!
        }
        if (substr($req_uri, 0, 5) === "/css/") {
            #!!! Faille de securité
            include_once $racine . path_os("/controleur/css.php"); #!!!
            #!!!
        }
        if (substr($req_uri, 0, 12) === "/javascript/") {
            #!!! Faille de securité
            include_once $racine . path_os("/controleur/javascript.php"); #!!!
            #!!!
        }
        if (substr($req_uri, 0, 7) === "/audio/") {
            #!!! Faille de securité
            include_once $racine . path_os("/controleur/immage.php"); #!!!
            #!!!
        }
}
