<?php
include_once $racine . path_os("/modele/bd_paneladmin.php");
$users = get_all_info_user();


$content_page = file_get_contents($racine.path_os("/vue/paneladmin.html"));


$liste_user = "";
foreach($users as $user) {
    $lineuser = file_get_contents($racine.path_os("/vue/paneladmin_lineuser.html"));
    $lineuser = str_replace("%id%", $user["id"], $lineuser);
    $lineuser = str_replace("%adressemail%", $user["adressemail"], $lineuser);
    $lineuser = str_replace("%nom%", $user["nom"], $lineuser);
    $lineuser = str_replace("%prenom%", $user["prenom"], $lineuser);
    $lineuser = str_replace("%numero%", $user["numero_tel"], $lineuser);
    $lineuser = str_replace("%grade%", $user["grade"], $lineuser);
    $liste_user = $liste_user.$lineuser;

}

$content_page = str_replace("%liste_utilisateur%", $liste_user, $content_page);

echo $content_page;
