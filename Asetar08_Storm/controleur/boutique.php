<?php


$head = '<link rel="stylesheet" type="text/css" href="'.$domaine.'/css/boutique.css" />
        <script type="text/javascript" src="'.$domaine.'/javascript/boutique.js"></script>
         <link rel="stylesheet" type="text/css" href="'.$domaine.'/css/navbar.css" />
         <link rel="stylesheet" href="http://www.asetar08.org:8080/css/footer.css" type="text/css" />';


include_once $racine.path_os("/vue/entete.html.php");

if(isset($_POST["add"]))
{
    if(isset($_SESSION["panier"]))
    {
        if(isset($_SESSION["panier"][$_POST["add"]]))
        {
            $_SESSION["panier"][$_POST["add"]] = $_SESSION["panier"][$_POST["add"]] + 1;
        }
        else
        {
            $_SESSION["panier"][$_POST["add"]] = 1;
        }
    }
    else
    {
        $_SESSION["panier"] = array($_POST["add"] => 1);
    }
}


include_once $racine.path_os("/modele/bd_boutique.php");

$page_article = file_get_contents($racine.path_os("/vue/boutique.html"));




$articles = get_all_article();
$liste_article = "";
foreach($articles as $article) {
    $articlep = file_get_contents($racine.path_os("/vue/boutique-article.html"));
    $articlep = str_replace("%Nom_Article%", $article["nom"], $articlep);
    $articlep = str_replace("%image%", $article["path_immage"], $articlep);
    $articlep = str_replace("%Prix%", $article["prix"], $articlep);
    $articlep = str_replace("%id_article%", $article["id"], $articlep);
    $articlep = str_replace("%description%", $article["description"], $articlep);
    $liste_article = $liste_article.$articlep;

}

$page_article = str_replace("%article%", $liste_article, $page_article);

if(isset($_SESSION["panier"]))
{
    $panierhtml = "<table><tr><td>Panier</td></tr><tr><td>Article</td><td>Quantité</td></tr>";
    foreach ($_SESSION["panier"] as $key => $qt)
    {
        $panierhtml = $panierhtml."<tr><td>".get_name_from_id($key)."</td><td>".$qt."</td></tr>";
    }
    $panierhtml = $panierhtml."</table>";
    $page_article = str_replace("%panier%", $panierhtml, $page_article);
}
else
{
    $page_article = str_replace("%panier%", "", $page_article);
}

echo $page_article;
