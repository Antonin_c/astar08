<?php

$head = '<link rel="stylesheet" type="text/css" href="'.$domaine.'/css/mesinfo.css" />
        <script type="text/javascript" src="'.$domaine.'/javascript/mesinfo.js"></script>
        <link rel="stylesheet" href="http://www.asetar08.org:8080/css/footer.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="'.$domaine.'/css/navbar.css" />';

include_once $racine . path_os("/vue/entete.html.php");
include_once $racine . path_os("/fonction_annexe/validateur_regex.php");
include_once $racine . path_os("/modele/bd_mesinfo.php");
include_once $racine . path_os("/modele/bd_login.php");

if (isset($_SESSION)) {
    try {
        $info = get_info_from_email($_SESSION["email"])[0];
        if (isset($_POST)) {
            if(isset($_POST["set_email"])) {
                if ($_POST["set_email"] != $_SESSION["email"]) {
                    if (email_valide($_POST["set_email"])) {
                        if (email_free($_POST["set_email"])) {
                            $id = get_id_from_email($_SESSION["email"]);
                            $r = set_info_where_id($_POST["set_email"], "adressemail", $id);
                            $_SESSION["email"] = $_POST["set_email"];
                            echo "L'email vien d'etre changer";
                        } else {
                            echo "Le mail n'est pas disponible";
                        }
                    } else {
                        echo "Erreur email invalide";
                    }
                }
            }
            if (isset($_POST["set_password_old"])&&isset($_POST["set_password_new"])) {
                if (($_POST["set_password_new"] != "") && ($_POST["set_password_old"] != "")) {
                    $r = login_password($_SESSION["email"], hash("sha256", $_POST["set_password_old"]));
                    if($r != false) {
                        if (password_secure($_POST["set_password_new"])[0] == true) {
                            $id = get_id_from_email($_SESSION["email"]);
                            $r = set_info_where_id(hash("sha256", $_POST["set_password_new"]), "password", $id);
                            echo "le password a été changer";
                        } else {
                            echo password_secure($_POST["set_password_new"])[1];
                        }
                    }
                }
            }
            if(isset($_POST["set_nom"])) {
                if ($_POST["set_nom"] != $info["nom"]) {
                    if ($_POST["set_nom"] != "") {
                        $id = get_id_from_email($_SESSION["email"]);
                        $r = set_info_where_id($_POST["set_nom"], "nom", $id);
                    } else {
                        echo "Il faut entrer un nom valide";
                    }
                }
            }
            if(isset($_POST["set_prenom"])) {
                if ($_POST["set_prenom"] != $info["prenom"]) {
                    if ($_POST["set_prenom"] != "") {
                        $id = get_id_from_email($_SESSION["email"]);
                        $r = set_info_where_id($_POST["set_prenom"], "prenom", $id);
                    } else {
                        echo "Il faut entrer un prenom valide";
                    }
                }
            }
            if(isset($_POST["set_numero"])) {
                if ($_POST["set_numero"] != $info["numero_tel"]) {
                    if (numero_valide($_POST["set_numero"])) {
                        if (num_free($_POST["set_numero"])) {
                            $id = get_id_from_email($_SESSION["email"]);
                            $r = set_info_where_id($_POST["set_numero"], "numero_tel", $id);
                        } else {
                            echo "numéro non disponible";
                        }
                    } else {
                        echo "Le numero de telephone n'est pas valide";
                    }
                }
            }
        }
    }
    catch (Exception $e)
    {
        echo "Erreur :" . $e;
    }
    finally
    {
        include_once $racine . path_os("/vue/mesinfo.html.php");
    }
}
else
{
    echo    "<h1>Vous devez etre connecter pour modifier vos information personelle</h1>";
    echo    '<form action="login.php" method="get">
            <input type="submit" value="login">
            </form>';
}

include_once $racine . path_os("/vue/pied.html.php");


