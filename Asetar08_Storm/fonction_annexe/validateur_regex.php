<?php

function email_valide($email)
{
    if(filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        return true;
    }
    else
    {
        return false;
    }
}



function numero_valide($numero)
{
    $regex = "/^[0-9]{10}$/";
    if(preg_match($regex, $numero) != false)
    {
        return true;
    }
    else
    {
        return false;
    }
}


function password_secure($password)
{
    $regex_minuscule = "/^.{0,}[a-z]{1,}.{0,}$/";
    $regex_majuscule = "/^.{0,}[A-Z]{1,}.{0,}$/";
    $regex_numero = "/^.{0,}[0-9]{1,}.{0,}$/";
    $regex_spec = "/^.{0,}\W{1,}.{0,}$/";
    $regex_len = "/^.{8,32}$/";
    if(preg_match($regex_minuscule, $password) != false)
    {
        if(preg_match($regex_majuscule, $password) != false)
        {
            if(preg_match($regex_numero, $password) != false)
            {
                if(preg_match($regex_spec, $password) != false)
                {
                    if(preg_match($regex_len, $password) != false)
                    {
                        return array(true, "Compte crée");
                    }
                    else
                    {
                        return array(false, "le mots de pass doit faire entre 8 et 32 charactere");
                    }
                }
                else
                {
                    return array(false, "il manque au moin un charactere spécial");
                }
            }
            else
            {
                return array(false, "il manque au moin un numero");
            }
        }
        else
        {
            return array(false, "il manque au moin une majuscule");
        }
    }
    else
    {
        return array(false, "il manque au moin une minuscule");
    }

}