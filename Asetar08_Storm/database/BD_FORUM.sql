#=====================================================================================
# BTS SIO - MYSQL
# ------------------------------------------------------------------------------------
# BD_FORUM.sql  (03/2022)
#=====================================================================================

DROP DATABASE IF EXISTS BD_FORUM;
CREATE DATABASE BD_FORUM;
USE BD_FORUM;

#=====================================================================================
#   TABLE : forum_sujets
#=====================================================================================
CREATE TABLE forum_sujets
(
    id int(6) NOT NULL auto_increment,
    auteur VARCHAR(30) NOT NULL,
    sujet text NOT NULL,
    date_derniere_reponse datetime NOT NULL default '0000-00-00 00:00:00',
    PRIMARY KEY (id)
);

#=====================================================================================
#   TABLE : forum_reponses
#=====================================================================================
CREATE TABLE forum_reponses
(
    id int(6) NOT NULL auto_increment, /* La commande AUTO_INCREMENT est utilisée dans le langage SQL afin de spécifier qu’une colonne numérique avec une clé primaire (PRIMARY KEY) sera incrémentée automatiquement à chaque ajout d’enregistrement dans celle-ci.*/
    auteur VARCHAR(30) NOT NULL,
    message text NOT NULL,
    date_reponse datetime NOT NULL default '0000-00-00 00:00:00',
    correspondance_sujet int(6) NOT NULL,
    PRIMARY KEY (id)
);

#=====================================================================================
# BTS SIO - MYSQL
# ------------------------------------------------------------------------------------
# BD_FORUM.sql  05/04/2022
#=====================================================================================