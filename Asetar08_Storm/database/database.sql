DROP DATABASE IF EXISTS database_Asetar08;
CREATE DATABASE IF NOT EXISTS database_Asetar08;
USE database_Asetar08;

CREATE TABLE compte
(
    id int not null AUTO_INCREMENT,
    adressemail  char(50),
    password char(64),
    nom  char(30),
    prenom  char(30),
    numero_tel  char(30),
    grade char(30),
    ban int,
    bandateoff date,
    PRIMARY KEY(id)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE utf8_bin;

INSERT INTO compte (id, adressemail, password, nom, prenom, numero_tel, grade) VALUES (1, "test@gmail.com", "b2e6c8f71c847dd0ebc643ca01e2f367d53ff060a8021e7ca1f23f3879e6c0a6", "nomtest", "prenomtest", "0123456789", "user"); #pass = passtest
INSERT INTO compte (id, adressemail, password, nom, prenom, numero_tel, grade) VALUES (2, "thomasschmittadresse@outlook.com", "ecf606b10e3ca79a44910fa52ea873b4c069340dc62b6a7a3711c177d3300bf8", "schmitt", "thomas", "0605127965", "admin"); #pass = passts


CREATE TABLE news
(
    date_publication DATETIME,
    titre varchar(60),
    path_immage varchar(60),
    path_texte varchar(60),
    PRIMARY KEY(date_publication)
);

INSERT INTO news (date_publication, titre, path_immage, path_texte) VALUES ("1991-04-21 17:50:01", "Sed minus facilis id adipisci quia sed delectus.", "/news/immage/1.jpg", "/news/texte/1.txt");
INSERT INTO news (date_publication, titre, path_immage, path_texte) VALUES ("2023-12-25 11:52:43", "Ad dolore distinctio ea suscipit itaque vel deserunt.", "/news/immage/2.jpg", "/news/texte/2.txt");
INSERT INTO news (date_publication, titre, path_immage, path_texte) VALUES ("2022-04-05 00:52:48", "In facilis quia et explicabo asperiores. ", "/news/immage/france.jpg", "/news/texte/3.txt");


CREATE TABLE article
(
    id integer not null,
    nom varchar(60),
    prix varchar (10),
    path_immage varchar(60),
    description varchar(800),
    PRIMARY KEY(id)
);

INSERT INTO article (id, nom, prix, path_immage, description) VALUES (1, "Chocolat AEM1", "3,10$", "/article/immage/1.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (2, "Chocolat Carfour2", "4.50$", "/article/immage/2.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (3, "Chocolat AEM3", "3,10$", "/article/immage/1.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (4, "Chocolat Carfour4", "4.50$", "/article/immage/2.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (5, "Chocolat AEM5", "3,10$", "/article/immage/1.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (6, "Chocolat Carfour6", "4.50$", "/article/immage/2.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (7, "Chocolat Carfour4", "4.50$", "/article/immage/2.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (8, "Chocolat AEM5", "3,10$", "/article/immage/1.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (9, "Chocolat Carfour6", "4.50$", "/article/immage/2.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (124, "Chocolat Carfour6", "4.50$", "/article/immage/2.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (10, "Chocolat Carfour4", "4.50$", "/article/immage/2.jpg","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (11, "Chocolat AEM5", "3,10$", "/article/immage/1.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ");
INSERT INTO article (id, nom, prix, path_immage, description) VALUES (123, "Chocolat Carfour6", "4.50$", "/article/immage/2.jpg", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
